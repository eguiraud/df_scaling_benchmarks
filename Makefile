ROOT_FLAGS := `root-config --libs --cflags --ldflags`
EXE := fat.x slim.x tbb_foreach.x tbb_map.x slim_less_false_sharing.x
.PHONY: clean all

all: $(EXE)

%.x: %.cpp
	g++ -o $@ $< $(ROOT_FLAGS) -O3 -g

clean: 
	rm -f $(EXE)
