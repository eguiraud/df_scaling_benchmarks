#include <iostream>
#include <chrono>
#include <ROOT/RDataFrame.hxx>

// NOTE: this removes false sharing but introduces a large amount of allocations,
// which is also bad

struct LargeDouble {
   char c[1024];
   LargeDouble &operator+(const LargeDouble& other) {
      return *this;
   }
   LargeDouble &operator+=(const LargeDouble& other) {
      return *this;
   }
};

int main(int argc, char* argv[])
{
  uint32_t threads=0;
  if(argc==2)
      threads=std::atoi(argv[1]);

  if (threads > 0)
    ROOT::EnableImplicitMT(threads);

  ROOT::RDataFrame r(100000000);
  auto rr = r.Define("v", [](ULong64_t e) {return LargeDouble{}; }, {"rdfentry_"}).Sum<LargeDouble>("v");

  auto start = std::chrono::high_resolution_clock::now();
  *rr;
  auto finish = std::chrono::high_resolution_clock::now();

  auto elapsed = finish-start;

  std::cout << threads << "\t" << elapsed.count() / 1e9 << std::endl;
}
