#include <ROOT/TThreadExecutor.hxx>
#include <chrono>
#include <iostream>
#include <cassert>

int Workload() {
  volatile int a = 0;
  for (int i = 0; i < 100; ++i)
    ++a;
  return 42;
}

int main(int argc, char** argv)
{
   const auto nEntries = 100000000;

   const auto nThreads = argc > 1 ? std::atoi(argv[1]) : 0;
   std::cout << nThreads << "\t";

   if (nThreads == 0) {
      const auto start = std::chrono::high_resolution_clock::now();
      for (int i = 0; i < nEntries; ++i)
         Workload();
      const auto finish = std::chrono::high_resolution_clock::now();
      const auto elapsed = finish - start;
      std::cout << elapsed.count() / 1e9 <<  std::endl;
      return 0;
   }

   ROOT::EnableImplicitMT(nThreads);
   ROOT::TThreadExecutor e(nThreads);
   const auto start = std::chrono::high_resolution_clock::now();
   auto results = e.Map(Workload, nEntries);
   const auto finish = std::chrono::high_resolution_clock::now();
   const auto elapsed = finish - start;
   std::cout << elapsed.count() / 1e9 << std::endl;

   for (auto e : results)
      assert(e == 42);

   return 0;
}
