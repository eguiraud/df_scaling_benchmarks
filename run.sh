#!/bin/bash

TO_RUN=""
ALL="fat.x slim.x tbb_map.x tbb_foreach.x"

if [[ $# -lt 1 ]] ; then
   TO_RUN="$ALL"
elif [[ $# -eq 1 ]] && [[ $1 == "--all" ]]; then
   TO_RUN="$ALL"
else
   while [[ ! -z $1 ]]; do
      case $1 in
      "--fat")
         TO_RUN="$TO_RUN fat.x"
         ;;
      "--slim")
         TO_RUN="$TO_RUN slim.x"
         ;;
      "--tbb_map")
         TO_RUN="$TO_RUN tbb_map.x"
         ;;
      "--tbb_foreach")
         TO_RUN="$TO_RUN tbb_foreach.x"
         ;;
      "--slim_less_false_sharing")
         TO_RUN="$TO_RUN slim_less_false_sharing.x"
         ;;
      "--help"|*)
         echo "usage:
   $0 --help
   $0 --all
   $0 [--fat] [--slim] [--tbb_foreach] [--tbb_map] [--slim_less_false_sharing]"
         exit 0
         ;;
      esac
      shift
   done
fi

for r in $TO_RUN; do
   echo "$r"
   for i in 0 1 2 4 8; do
      ./$r $i
   done
done
