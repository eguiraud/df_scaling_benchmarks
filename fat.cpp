#include <ROOT/RDataFrame.hxx>
#include <chrono>
#include <iostream>

double FatDefine() {
  volatile int a = 0;
  for (int i = 0; i < 100; ++i)
    ++a;
  return 42;
}

int main(int argc, char** argv)
{
   const auto nThreads = argc > 1 ? std::atoi(argv[1]) : 0;
   if (nThreads > 0)
      ROOT::EnableImplicitMT(nThreads);
   std::cout << nThreads << "\t";

	ROOT::RDataFrame df(100000000);
	auto result = df.Define("x", FatDefine).Sum<double>("x");
	
	const auto start = std::chrono::high_resolution_clock::now();
	*result;
	const auto finish = std::chrono::high_resolution_clock::now();
	const auto elapsed = finish - start;
	std::cout << elapsed.count() / 1e9 << std::endl;

	return 0;
}
