#include <iostream>
#include <chrono>
#include <ROOT/RDataFrame.hxx>

int main(int argc, char* argv[])
{
  uint32_t threads=0;
  if(argc==2)
      threads=std::atoi(argv[1]);

  if (threads > 0)
    ROOT::EnableImplicitMT(threads);

  ROOT::RDataFrame r(100000000);
  auto rr = r.Define("v", [](ULong64_t e) {return pow(1./(e+1),2);}, {"rdfentry_"}).Sum<double>("v");

  auto start = std::chrono::high_resolution_clock::now();
  *rr;
  auto finish = std::chrono::high_resolution_clock::now();

  auto elapsed = finish-start;

  std::cout << threads << "\t" << elapsed.count() / 1e9 << std::endl;
}
